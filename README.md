
# Instructions


How to pull and execute a script from this repo.


- get the RAW url by clicking on the script and then on raw button to the upper right side.
- `curl -fsSL '<raw-url>' -o mynewname-script.sh`
- `chmod +x mynewname-script.sh`
- execute the script: `./mynewname-script.sh`


You can also do:

`curl '<raw url>' | bash`
