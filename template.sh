#!/usr/bin/env bash

#{{{ Bash settings
# abort on nonzero exitstatus
set -o errexit
# abort on unbound variable
#set -o nounset
# don't hide errors within pipes
set -o pipefail
#}}}

#{{{ Variables
VERSION=v0.1.0
IFS=$'\t\n'   # Split on newlines and tabs (but not on spaces)
script_name=$(basename "${BASH_SOURCE[0]}")
XMAR_WORKDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export XMAR_WORKDIR
readonly script_name XMAR_WORKDIR


#}}}


cli_help_deploy() {
    echo "
Command: deploy

Usage:
    deploy project_name"
    exit 1
}

deploy() {
    [[ -z "$1" ]] && cli_help_deploy
    cli_log "Deployment BEGIN"
    
    cli_log "ENV variables"
    env | grep "XMAR_*"
    
    cli_log "Executing helm upgrade --install ..."
    # Example
    # helm upgrade --install \
    #   --wait \
    #   --values $BAGCLI_PROJECTS_PATH/$BAGCLI_PROJECT_NAME/values.yaml \
    #   --namespace $BAGCLI_K8S_NAMESPACE $BAGCLI_PROJECT_NAME $BAGCLI_REPO_PATH
    
    cli_log "Deployment END"
}

main(){
    # check_args "${@}"
    :
    
    case "$1" in
        deploy|d)
            deploy "$2" #| tee -ia "$WORKDIR/logs/deploy_${2}.log"
        ;;
        *)
            cli_help
        ;;
    esac
}

#{{{ Helper functions
cli_log() {
    # script_name=${0##*/}
    timestamp=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
    echo "== $script_name $timestamp $1"
}

cli_help() {
    cli_name=${0##*/}
    echo "
$cli_name
my CLI
Version: ${VERSION}
https://github.com/xmarlem/mycli

Usage: $cli_name [command]

Commands:
  deploy    Deploy
  *         Help
    "
    exit 1
}

cleanup() {
    :
    
    ## if required uncomment this
    # ERROR_CODE="$?"
    # [[ $ERROR_CODE != 0 ]] && printf -- "an error occurred. cleaning up now... "
    # printf "cleaning up..."
    # # ... cleanup code ...
    # printf -- "DONE.\nExiting with error code %d .\n", ${ERROR_CODE}
    # exit ${ERROR_CODE}
}

trap "cleanup" EXIT

#}}}


main "${@}"
